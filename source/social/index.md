-----
layout: page
menu_id: social
breadcrumb: false
-----
{% navbar active:/social/ [友情链接](/social/) [留言板](/social/comment/) %}

{% note color:red 友链信息补全计划：
如下所示，旧友链缺少了站点截图和描述信息，如果您有需要，请在下方评论完善您的站点信息，具体请参考收录格式（如下）

```yaml
- title: 
# 站点标题
  url:
# 站点链接
  cover:
# 站点截图（PC）
  icon:
# 站点头像
  description:
# 站点描述
```

完善后可显示完整站点信息，同时建议新收录站点也提交完整信息收录。
%}

{% friends link %}

{% note color:green 来交换友链吧！Mikuの次行星欢迎各方站点递交信息互相收录！请注意先在您的站点添加本站信息后再通过评论区或博主邮箱申请收录！

本行星的信息如下：

```yaml
- title: Mikuの次行星
# 站点标题
  url: https://tkgso.fun
# 站点链接
  cover: 站点截图待完善，如有需求可自行截取
# 站点截图（PC）
  icon: https://cravatar.cn/avatar/ba76faef3cc8252bd0433b3515124389?s=512
# 站点头像
  description: 心有多宽，世界就有多远
# 站点描述
```

%}