-----
title: 个人人际交往杂谈
date: 2024-07-14 20:40
categories: 生活
tags: [人际,杂谈]
abbrlink: 3
-----

我是一个00后，我出生在2006年，今年刚好18岁了，我属于那批后00后的年轻人了。我认识的，我见过的，那些跟我差不多大的人，他们出去玩都喜欢干什么呢？唱KTV，打台球，耍车。我个人属于不怎么擅长现实交际的，在现实里不喜欢外出参与我刚才说的这类活动。

抽烟，喝酒，嚼槟榔。我身边认识的一些人他们常常向我安利这些活动。我个人偶尔会喝点啤酒，比如家里吃些油腻的饭菜啊，给我的朋友庆祝生日，家里举办活动都会喝点。但除此之外，我不想抽烟，也不想把酒当水喝，更不想嚼槟榔。

但是我认识的这些朋友，当然可以说我只是点出了我认识的一部分“融入社会”的朋友，不是说我身边只有这样的朋友。他们常常属于那些我们年轻人认为的“成功人士”，这个成功人士不是说那种商业层面的，而是从我们这一代认知层面的。他们常常跟我说，你不抽烟，很多事情很难办。这是我一个“成功”朋友说的，他可以说确实符合现代我们认识的成功，有肤白貌美的女朋友，两天收入一部苹果15 Pro的金钱，他似乎比一些90后打工者，00后打工者都要成功，毕竟这些打工者忙活一个月可能才赚出了他两天忙活的钱。

在我看来，他这个成功就是“投机倒把”，这只能说是我的个人观点，但他跟我“装逼”装的天花乱坠，还建议我也来干，我自己也在假期干点小兼职，送外卖什么的。

他自己跟我说的那些工作包括了以下内容：
- “发短信”：跟我说给别人发短信，用自己的手机号发，发一条给五六块钱，一天发上几百条，那就是发的短信条数乘以五的RMB入账。一听就知道应该不是正经短信，当我想要询问他发的什么短信时遭到了他的拒绝，他就说你干不干吧，我说违法的不干，他说谁抓的到你？我反正推脱了一下，表示了我不干的意思。
- “卖孩子”：肯定不是我们理解的那个卖孩子，如果真是那个卖孩子，在当下环境看，我这个朋友应该是坐在号子里跟我说这个。我想要具体了解内容时也遭到了他的拒绝，他的意思是你要干我才跟你说。我并没有兴趣做什么“地下记者”。去接受这类不知道什么东西然后揭露他们。我含蓄的表示了我的拒绝。后来听别人讲起，大概可能是“投机倒把”，倒卖自行车。虽然不能理解为什么这玩意被叫做“卖孩子”，但应该是个合法行业？（~~倒卖自行车两天换苹果15 Pro在我看来有点过于梦幻~~）

然后，我前边也说过了，他建议我学会抽烟，说你不抽这一口，很多事情很难办。这我大概有些体会：我在兼职干外卖的时候，有一回我的电动车坏掉了，当时在骑手群里有位大哥问讯跑了五公里来帮忙（真的很感谢这位大哥，好人一生平安），没解决车子的问题，我俩在坐着闲聊的时候他下意识给我递了根烟，我说我不抽烟，他感慨：“好孩子啊！”我的表情大概可能是这样：😅。

不过这也反映了几点：

- 在他认知里，快速拉近社交距离的方式是递根烟。我们可以说这是寻找共同点的一种方式 ，你抽烟，我也抽烟，我俩就有这么个共同点。然后就可以开始聊天了。
- 我如果接下这根烟，就像在酒吧里相遇递杯酒一样，大家手里有这么个消磨情绪或者说表示身份的东西，显得很正常，我不接就显得有些怪异。
- 这是他开始聊天的习惯性动作，证明他身边有好友，或者经常社交时都是互相递烟，烟已经成为了一种社交品。递烟的时候也可以问问你这是什么牌子，好不好吃，在什么地方买的，花了多少钱买的？

以上几点其实也反映了人际社交基本的事情：“有共同点，找共同话题。”

没共同点怎么打开这个话题？不好开，两个世界的人难以凑到一块，就像别人给你说今天股市怎么样，你跟他说今天玩了什么游戏，玩的怎么样一样。没有个交流基本开头，是难以开始聊天的。更不用说在现在这个人人都带个面具的时代完成“交心”式聊天。

我厌恶这种人际交流方式，我的很多社交都是在网络上完成的，在网上我认识了不少志同道合的人。比如我喜欢番剧《擅长捉弄的高木同学》，通过它的QQ频道，其他社群我认识了一批网友，我们也有不少共同话题。

这就引出了我认为人际交往中的一个东西：“有一个共同点的人也有不少共同点。”这只是我的一个个人观点罢了。

总之说了这么多，其实我也是想吐槽一下现在人对于人际关系的观点，但是吐槽完了，似乎我还是要带上面具去处理这些人际关系，虽然我不会去抽烟，但是我肯定还是要想别的办法去融入现实人际交往，我不可能完全拒绝现实人际交往，除非我完全居家。

好，就先写到这里吧，先说明，以上这些都只是我的个人观点，如果你有什么疑问或者认为我写的有什么问题，欢迎邮件联系我或评论区指正。