---
wiki: flojoy
title: 赞助与反馈
---

{% note color:cyan 纷语驿站目前是一个非商业化项目，你的每一笔赞助都将成为我们的维持下去的动力，同时你也可以获得『赞助者』头衔，解锁更多照片存储空间。 %}

## 爱发电（支持微信/支付宝/PayPal）:

{% button 发电 https://afdian.net/order/create?user_id=c01a2da6e3dd11ed95565254001e7c00&remark= icon:lightly %}

## 虚拟货币

{% note color:error 注意！ 若使用此种方式，请确保发出货币的网络为公网，否则无法成功
%}

### 比特币：

{% copy 3PkhDihVXAEurdb3z32NnoD77NH77kiDHh %}

### ETH：

{% copy 0x10357542C10eFB8689BDf1d57f2E357126B39911 %}

## 反馈与联系

{% note color:warning 免责声明 出于您咨询问题的具体情况，通常情况下并非由我们造成的问题是没法被有效解决的，但如果确实有相关的需求，我们可以一同参与寻找解决方案。
%}

### 联系方式

{% note color:warning 我们也并非全职客服，维护本站仅仅是运维组闲暇生活的一部分，所以请恕我们也许未必能及时回答您的问题，如非必要，请不要私聊群内其他用户，以避免带来困扰。 %}

用户QQ群：863463228
站内可私信公号：@offical@flojoy.fun
反馈邮箱：info@flojoy.fun

{% button 下一步 ./niceend.html icon:next %}
