---
wiki: flojoy
title: 客户端
---

{% note color:green Misskey集成了PWA模块，可以在支持的环境上将网页作为应用程式安装。
安装后可自动支持推送通知功能，方便您快速使用。 
%}

但其原生自带的网页客户端的依赖比较庞大，运行时间长后对系统的资源和负载有不小的要求。因此我们推荐您使用支持 ActivityPub 协议的第三方客户端。

如果您使用的是 Apple 生态的产品（如 iPhone 、 iPad 或是 Mac ），我们推荐您使用 Kimis

{% link https://apps.apple.com/app/kimis-a-client-for-misskey/id1667275125 %}

对于安卓设备，我们推荐使用 Kaiteki 或 Milktea

{% link https://kaiteki.app/ desc:true %}

{% link https://github.com/pantasystem/Milktea/releases/ desc:true %}

{% button 下一步 ./contribute.html icon:next %}