---
wiki: flojoy
title: 快速开始
---

> 纷语驿站是由木创社·木语星运营的 Fediverse 去中心化社区站点，基于 Misskey 的边缘小酒吧。ACGN、日常生活... 包容欢迎每个人的真诚分享。

目前站点状态：{% mark 开放注册 color:green %}

{% image https://pic.imgdb.cn/item/6683c954d9c307b7e9d774e8.webp 使用截图 %}

{% note color:cyan 你准备好加入纷语驿站了吗？准备好了就点击下方按钮开始吧！ %}

{% button 快速开始 ./agreement.html icon:start %}

{% button 使用文档 https://docs.flojoy.fun icon:books %}
