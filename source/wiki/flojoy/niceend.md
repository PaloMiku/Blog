---
wiki: flojoy
title: 欢迎使用
---

恭喜🎉，你完成了纷语驿站快速引导，前往站点吧！

{% button 前往纷语驿站 https://flojoy.fun icon:start %}