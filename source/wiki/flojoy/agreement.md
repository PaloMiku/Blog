---
wiki: flojoy
title: 使用须知
---

使用纷语驿站相关服务前，请务必阅读『服务条款（TOS）』与『合理使用规约（AUP）』
{% link https://docs.flojoy.fun/tos/ 服务条款  desc:true %}

{% link https://docs.flojoy.fun/aup/ 合理使用规约  desc:true %}

请使用Edge,Firefox,Chrome最新版本访问纷语驿站以保证站点正常访问。站点位于海外服务器，部分时间段中国大陆地区可能访问受阻，请耐心等待或自行切换网络环境。

{% note color:red 进行“下一步”操作即代表你同意上述相关须知及其相关内容。 %}

{% button 下一步 ./application.html icon:next %}
