---
layout: page
menu_id: more
breadcrumb: false
comments: false
---

{% navbar active:/more/pay/ [版权信息](/more/copyright/) [隐私政策](/more/privacy-policy/)
 [关于博主](/more/) [赞助博主](/more/pay/) %}

![微信赞赏码](https://npm.elemecdn.com/yzsong06@latest/source/image/Pay.png)

## 介绍

**欢迎通过以上微信赞赏码为我捐款。每一笔捐款都是我持续维护网站和生产内容的动力！**

## 列表

| 赞助者  | 金额（价值）| 途径 | 时间 | 
| ------------ | ------------ | ------------ | ------------ |
| 柠檬君 | ￥30 | 微信 | 2022 年 12 月 |
| 南栀  | ￥10  | QQ | 2023 年 1 月 |
| XXQY | ￥20  | 微信 | 2023 年 5 月 |
| **[WaterApple](https://waterapple09.com)**  | **$16.98**  | 设备赞助 | 2023 年 12 月 |
| 母亲 | ￥40  | 微信 | 2024 年 2 月 |
| 总金额 | 约￥240 |